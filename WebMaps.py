# -*- coding: utf-8 -*-
"""
Created on Sat Dec 23 14:44:46 2017
(         )
@author: jites
"""
import folium as fo
map=fo.Map(location=[20,85],zoom_start=5)
list1=[]

list1=range(20,30)
list2=range(75,85)
list3=range(320,330)
set4=[]

for i,j,k in zip(list1,list2,list3):
    
    set1=[i,j,k]
    set4.append(set1)
def colordef(var):
    if 319<var<323:
        return "green"
    elif 323<var<327:
        return "orange"
    elif var>327:
        return "green"
    
fg=fo.FeatureGroup(name="My map")
fgp=fo.FeatureGroup(name="Population map")


for coordinates in set4:
    fg.add_child(fo.CircleMarker(location=coordinates,radius=16,fill_opacity=1,popup=str(coordinates[2]) ,color=colordef(coordinates[2])))

fgp.add_child(fo.GeoJson(data=open('world.json','r+',encoding='utf-8-sig').read(),style_function=
lambda x: {"fillColor": "green" if x["properties"]["POP2005"] < 10000000 
else 'orange' if 10000000 <= x["properties"]["POP2005"] < 20000000 else "red" }))

    


map.add_child(fg)
map.add_child(fgp)
map.add_child(fo.LayerControl())



map.save("MapCreated.html")
print(map)































